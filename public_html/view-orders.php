<?php
include('_layout.php');
include("check-if-login.php");
echoLayoutTop();
?>

<html>

	<head>

		<meta charset="utf-8">
		<title>Flowboard Order Form</title>

		<meta name="description" content="This is the Flowboard Order Form using the Flat UI Toolkit."/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<!-- Loading Bootstrap -->
		<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

		<!-- Loading Flat UI Pro -->
		<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

		<!-- LOADING CUSTOM CSS -->
		<link href="custom_styles.css" rel="stylesheet">

		<link rel="shortcut icon" href="img/favicon.ico">


		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
			<script src="dist/js/vendor/html5shiv.js"></script>
			<script src="dist/js/vendor/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<?php
			echoNavLinks();
		?>
			
		<br/>

		<div class="demo-headline" id="header" id="movetoback">
			<h1 class="title">
				<div class="title"></div>
				StudentPrint
				<small>Orders</small>
			</h1>
		</div>

		<div class="container">
			
			<!-- FAKE DISPLAY LINKS -->
			<!--<p class="navbar-text pull-right" id="movetofront">
				<a href="#">staff page</a> |
				<a href="view-orders.php">view orders</a> |
				<a href="logout.php">log out</a>
			</p>-->

			<!-- AWESOME TITLE BUT MAYBE FIX THE FORMATTING UP A BIT -->

			<form action"" method="post">

				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered"> <!-- other options: table-striped -->
							<thead>
	              <tr>
	                <th>Order #</th>
									<th>Name</th>					
									<!--<th>Phone</th>-->
									<th>Due Date</th>
									<!--<th>Email</th>-->	
									<!--<th>Pages</th>
									<th>Copies</th>					
									<th>Color Print</th>
									<th>Double Sided</th>
									<th>Paper Size</th>
									<th>Paper Color</th>
									<th>Weight</th>
									<th>Finishing</th> -->
									<th>Payment Method</th>
									<th>Status</th>
									<!--<th>Comments</th>-->
									<th></th>
									<th></th>
									<th></th>
	              </tr>
	            </thead>
	            <tbody>
								<?php
									//get database
									include("db-connect.php");
									$conn = dBConnect();
									// query database for orders --> result
									$result =  $conn->query("SELECT * FROM orders"); 
									// loop through results of database query, displaying them in the table
									if ($result->num_rows > 0) {
									// output data of each row
											while($row = $result->fetch_assoc()) {				
											// echo out the contents from the database of each row into a table
											echo '<tr>';
											echo '<td>' . $row['id'] . '</td>';
											echo '<td>' . $row['name'] . '</td>';
											//echo '<td>' . $row['phone'] . '</td>';
											echo '<td>' . $row['due_date'] . '</td>';
											//echo '<td>' . $row['email'] . '</td>';
											/*echo '<td>' . $row['numOfPages'] . '</td>';
											echo '<td>' . $row['numOfCopies'] . '</td>';
											echo '<td>' . $row['color'] . '</td>';
											echo '<td>' . $row['doubleSided'] . '</td>';
											echo '<td>' . $row['paper_size'] . '</td>';
											echo '<td>' . $row['paper_color'] . '</td>';
											echo '<td>' . $row['weight'] . '	</td>';
											echo '<td>' . $row['finishing']. '</td>';		*/			
											echo '<td>' . $row['payment_method']. '</td>';
											echo '<td>' . $row['status']. '</td>';
											//echo '<td>' . $row['comments']. '</td>';
											

											echo "<td><a href='view-order.php?id=".$row['id']."'>View</a></td>";											
											
											echo "<td><a href='edit-orders.php?id=".$row['id']."' onclick='return confirm(\"Are you sure you want to do this?\")'>Edit</a></td>";
												
											
											echo "<td><a href='remove-orders.php?id=".$row['id']."' onclick='return confirm(\"Are you sure you want to do this?\")'>Delete</a></td>";
											echo '</tr>';	
										}    
									}
								?>
							</tbody>
						</table>
					</div>
				</div>

			</form>

		</div>

		<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
		<script src="../dist/js/vendor/jquery.min.js"></script>
		<script src="../dist/js/vendor/video.js"></script>

		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="../dist/js/flat-ui-pro.min.js"></script>

		<script src="../dist/js/application.js"></script>

		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('table').dataTable();
			} );
		</script>
		
	</body>

</html> 


<?php
echoLayoutBottom();
?>  