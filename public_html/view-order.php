<?php 
include('_layout.php'); 
include("check-if-login.php");
echoLayoutTop(); ?>


<?php

// connect to the database
include('db-connect.php');
$conn = dbConnect();
 
// check if the form has been submitted. If it has, process the form and save it to the database
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{ 

	// confirm that the 'id' value is a valid integer before getting the form data
	if (is_numeric($_POST['id']))
	{

			//Get form data to make sure it's valid
		$id = $_POST["id"];
		$name = $_POST['name'];
		$dueDate = $_POST['dueDate'];
		$email = $_POST['email'];
		$numOfPages = $_POST['numOfPages'];
		$numOfCopies = $_POST['numOfCopies'];
		$paperSize = $_POST['paperSize'];
		$paperColor = $_POST['paperColor'];
		$weight = $_POST['weight'];
		$finishing = $_POST['finishing'];
		$paymentMethod = $_POST['paymentMethod'];
		$printColor = $_POST['printColor'];
		$status = $_POST['status'];
		$comment = $_POST['comment'];
	 
		 
	}
	
	}
	else {
		if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
			// query db
			$id = $_GET['id'];
			$sql = "SELECT * FROM orders WHERE id = '$id'";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			// output data of each row
					$row = $result->fetch_assoc();
					// get data from db
				$id = $row['id'];
				$name = $row['name'];
				$email = $row['email'];
				$phone = $row['phone'];
				$dueDate = $row['due_date'];
				$numOfPages = $row['numOfPages'];
				$numOfCopies = $row['numOfCopies'];
				$paperSize = $row['paper_size'];
				$paperColor = $row['paper_color'];
				$weight = $row['weight'];
				$finishing = $row['finishing'];
				$paymentMethod = $row['payment_method'];
				$printColor = $row['color'];
				$status = $row['status'];
				$comment = $row['comments'];
		
			}

/*
		echo '<td>' . $id . '</td>';
		echo '<td>' . $name . '</td>';
		echo '<td>' . $due_date . '</td>';
		echo '<td>' . $status . '</td>';

		echo '<td>' . $phone . '</td>';
		echo '<td>' . $email . '</td>';

		echo '<td>' . $numOfPages . '</td>';
		echo '<td>' . $numOfCopies . '</td>';
		echo '<td>' . $color . '</td>';
		echo '<td>' . $doubleSided . '</td>';
		echo '<td>' . $paper_size . '</td>';
		echo '<td>' . $paper_color . '</td>';
		echo '<td>' . $weight . '	</td>';
		echo '<td>' . $finishing . '</td>';	


		echo '<td>' . $payment_method . '</td>';
		echo '<td>' . $comments . '</td>';
*/											

	
		}
	}
?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Order Details</title>

	<meta name="description" content="This is Flowboard - Edit Order using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<!--<div class="container"><section id="block-text"><div class="block-text">-->

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Order Details</small>
				</h1>
			</div>

			<!--</div></section></div>-->

			<!--<div class="container">-->
				<div class="row">
					<div class="col-lg-8">
						<div class="panel panel-default">
							<!-- Default panel contents -->
							<div class="panel-heading">Order # <?php echo $id?>: <?php echo $status?></div>
							<!--<div class="panel-body">
								<p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
							</div>-->

							<!-- Table -->
							<table class="table">
								<tbody>
									<!--<tr>
										<td>Order #</td>
										<td><?php echo $id?></td>
									</tr>-->
									<tr>
										<td>Name</td>
										<td><?php echo $name?></td>
									</tr>
									<tr>
										<td>Due Date</td>
										<td><?php echo $due_date?></td>
									</tr>
									<!--<tr>
										<td>Status</td>
										<td><?php echo $status?></td>
									</tr>-->
									<tr>
										<td>Email</td>
										<td><?php echo $email?></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td><?php echo $phone?></td>
									</tr>
									<tr>
										<td># Pages</td>
										<td><?php echo $numOfPages?></td>
									</tr>
									<tr>
										<td># Copies</td>
										<td><?php echo $numOfCopies?></td>
									</tr>
									<tr>
										<td>Color</td>
										<td><?php echo $color?></td>
									</tr>
									<tr>
										<td>DoubleSided?</td>
										<td><?php echo $doubleSided?></td>
									</tr>
									<tr>
										<td>Size</td>
										<td><?php echo $paper_size?></td>
									</tr>
									<tr>
										<td>Color</td>
										<td><?php echo $paper_color?></td>
									</tr>
									<tr>
										<td>Weight</td>
										<td><?php echo $weight?></td>
									</tr>
									<tr>
										<td>Finishing</td>
										<td><?php echo $finishing?></td>
									</tr>
									<tr>
										<td>Payment Method</td>
										<td><?php echo $payment_method?></td>
									</tr>
									<tr>
										<td>Comments</td>
										<td><?php echo $comments?></td>
									</tr>
									<tr>
										<td>Files someday</td>
										<td><!--<?php echo $STUFF?>--></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			<!--</div>-->

	</div>

	<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
	<script src="../dist/js/vendor/jquery.min.js"></script>
	<script src="../dist/js/vendor/video.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../dist/js/flat-ui-pro.min.js"></script>

	<script src="../dist/js/application.js"></script>

</body>

<?php echoLayoutBottom(); ?>

</body>

</html>