<?php
include('_layout.php');
echoLayoutTop(); ?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Edit User</title>

	<meta name="description" content="This is Flowboard - Edit User using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<div class="container"><section id="bigger-block-text"><div class="bigger-block-text">

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Reset Password</small>
				</h1>
				<br/><br/>
			</div>

			<form action="" method="POST" class="form-horizontal" role="form"><!-- action"" ? -->

				<div class="form-group">
					<h6>To reset your password, please enter your email address.</h6> 
				</div>

				<div class="form-group">
						<input type="email"  name="email" autofocus="autofocus" class="form-control" placeholder="Email Address">
				</div>

				<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-block" value="reset password">Reset Password</button>
				</div>

				<!--
				<div class="form-group">
					<label class="col-sm-4 control-label" for="email">Enter your email address</label>
					<div class="col-sm-8">
						<input type="email"  name="email" autofocus="autofocus" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label" for="edituser"></label>
					<div class="col-sm-8">
						<button type="submit" class="btn btn-primary btn-lg btn-block" value="reset password">Reset Password</button>
					</div>
				</div>
				-->

			</form>

		</div></section></div>

	</div>

<?php
	function randomPassword() {
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
					$n = rand(0, $alphaLength);
					$pass[] = $alphabet[$n];
			}
			return implode($pass); //turn the array into a string
	}

	if(isset($_POST['email'])){
		$email = $_POST['email'];

		$password = randomPassword();
		$hashedPass = md5($password);

		include('db-connect.php');
		$conn = dbConnect();

		$sql = " SELECT * FROM employees WHERE email='$email' ";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {

			$sql = " UPDATE employees SET password = '$hashedPass' WHERE email='$email' ";
			$result = $conn->query($sql);

			include('mail.php');
			$sub = "Your new password";
			$msg = "The new password is $password.";
			WebmasterMail($email, $sub, $msg);

			echo "<p>Your new password has been sent to your email account</p>";
			echo "<p><a href='view-users.php'>Go back</a></p>";
		} else {
			echo "<p>No account with this email was found.</p>";
		}

	}
?>

<?php echoLayoutBottom(); ?>

