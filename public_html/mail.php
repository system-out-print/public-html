<?php
function WebmasterMail($email, $sub, $msg){
	require 'PHPMailer-master/PHPMailerAutoload.php';
	$status;

	$mail = new PHPMailer;

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'mail.spflowboard.com';				  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'noreply@spflowboard.com';        // SMTP username
	$mail->Password = 'SPFl0wB0ard!';                     // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->From = 'noreply@spflowboard.com';
	$mail->FromName = 'noreply@spflowboard.com';
	$mail->addAddress($email);     						  // Add a recipient

	$mail->Subject = $sub;
	$mail->Body    = $msg;

	if(!$mail->send()) {
	    $status = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    $status = 'Message has been sent';
	}

	return $status;
}