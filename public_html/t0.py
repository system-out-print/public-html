#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb
import sys

connection = MySQLdb.connect (host = 'localhost', user='ab78518_admin', passwd = 'fl0wb0ard!', db = 'ab78518_spflowboard')

cursor = connection.cursor ()

cursor.execute ('SELECT VERSION()')

row = cursor.fetchone ()

print 'Server version:', row[0]

cursor.close ()

connection.close ()

sys.exit()
