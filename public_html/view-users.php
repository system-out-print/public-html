<?php
include('_layout.php');
include("check-if-admin.php");
echoLayoutTop();
?>

<html>

	<head>

		<meta charset="utf-8">
		<title>Flowboard Order Form</title>

		<meta name="description" content="This is the Flowboard Order Form using the Flat UI Toolkit."/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<!-- Loading Bootstrap -->
		<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

		<!-- Loading Flat UI Pro -->
		<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

		<!-- LOADING CUSTOM CSS -->
		<link href="custom_styles.css" rel="stylesheet">

		<link rel="shortcut icon" href="img/favicon.ico">


		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
			<script src="dist/js/vendor/html5shiv.js"></script>
			<script src="dist/js/vendor/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<?php
			echoNavLinks();
		?>

		<br/>

		<!-- AWESOME TITLE BUT MAYBE FIX THE FORMATTING UP A BIT -->
		<div class="demo-headline" id="header" id="movetoback">
			<h1 class="title">
				<div class="title"></div>
				StudentPrint
				<small>Employees</small>
			</h1>
		</div>


		<div class="container">

			<!-- FAKE DISPLAY LINKS -->
			<!--<p class="navbar-text pull-right" id="movetofront">
				<a href="view-users.php">view employees</a> |
				<a href="view-orders.php">view orders</a> |
				<a href="logout.php">log out</a>
			</p>-->


			<form action"" method="post">

				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered"> <!-- other options: table-striped -->
							<thead>
	              <tr>
									<th>User Name</th>
									<th>First Name</th> 
									<th>Last Name</th>
									<th>email</th>
									<th>admin</th> 
									<!--<th>password</th>-->
									<th></th>
									<th></th>
									<th></th>
	              </tr>
	            </thead>
	            <tbody>

								<?php

								if ($_SERVER['REQUEST_METHOD'] === 'GET') 
								{
									if(isset($_GET['statusMsg']))
									{
										if($_GET['statusMsg'] == "new-user"){
											echo "<p>New employee added.</p>";
										}else if($_GET['statusMsg'] == "edit-user") {
											echo "<p>Employee has been edited.</p>";
										}
									}
								}

								include("db-connect.php");
								$conn = dbConnect();
								// query database for employees --> result
								$result =  $conn->query("SELECT * FROM employees"); 
								// loop through results of database query, displaying them in the table
								if ($result->num_rows > 0) {
								// output data of each row
										while($row = $result->fetch_assoc()) 
										{	
										// echo out the contents of each row into a table
										echo '<tr>';
										echo '<td>' . $row['username'] . '</td>';
										echo '<td>' . $row['first_name'] . '</td>';
										echo '<td>' . $row['last_name'] . '</td>';		
										echo '<td>' . $row['email'] . '</td>';
										echo '<td>' . $row['admin'] . '</td>';
										//echo '<td>' . $row['password'] . '</td>';
										
										
										echo "<td><a href='edit-users.php?id=".$row['id']."' onclick='return confirm(\"Are you sure you want to do this?\")'>Edit</a></td>";
										
										echo "<td><a href='delete-orders.php?id=".$row['id']."' onclick='return confirm(\"Are you sure you want to do this?\")'>Delete</a></td>";
										
										echo "<td><a href='reset-pass.php?id=".$row['id']."' onclick='return confirm(\"Are you sure you want to reset user's password?\")'>Reset Password</a></td>";	
										
										echo '</tr>';
									}
								} 
								?>

							</tbody>
						</table>
					</div>
				</div>

				<a href="add-users.php">Add an Employee</a>
			</form>

		</div>

		<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
		<script src="../dist/js/vendor/jquery.min.js"></script>
		<script src="../dist/js/vendor/video.js"></script>

		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="../dist/js/flat-ui-pro.min.js"></script>

		<script src="../dist/js/application.js"></script>

		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('table').dataTable();
			} );
		</script>
		
	</body>

</html> 


<?php
	echoLayoutBottom();
?>