<?php
session_start();
$_docRoot = $_SERVER["DOCUMENT_ROOT"];

function echoLayoutTop(){ 
	?>
		<html>
		<head>
			<link rel="stylesheet" type="text/css" href="style/style.css">
			<link rel="stylesheet" type="text/css" href="style/table.css">
			<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		</head>
		<body>
	<?php
}

function echoNavLinks(){ 

	echo '<p class="navbar-text pull-right">';

	if(isset($_SESSION['loggedIn'])) {
		if($_SESSION['admin'] == 1) {

			// nav bar for admin
			echo '<a href="view-users.php" id="layout-link">view employees |</a>';
			echo '<a href="view-orders.php"> view orders |</a>';
			echo '<a href="change-password.php"> change password |</a>';
			echo '<a href="logout.php"> log out</a>';
		}
		else if($_SESSION['loggedIn'] == 1) {

			// nav bar for employees
			echo '<a href="view-orders.php">view orders |</a>';
			echo '<a href="change-password.php"> change password |</a>';
			echo '<a href="logout.php"> log out</a>';
		} 	
	} else {
		$host = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		if (($host == 'spflowboard.com/login.php') || ($host == 'www.spflowboard.com/login.php')) {
			// redirect from login screen to order page
			echo 'not studentprint staff?  <a href="index.php" id="layout-link"> place an order</a>';
		} else if (($host == 'spflowboard.com/reset-password.php') || ($host == 'www.spflowboard.com/reset-password.php')) {
			echo 'studentprint staff? <a href="login.php"> log in </a> &nbsp; |';
			echo ' &nbsp; not studentprint staff?  <a href="index.php" id="layout-link"> place an order</a>';
		} else{
				// if not logged in, offer login
				echo 'studentprint staff? <a href="login.php">  log in</a>';
		}
	}

	echo '</p>';

}

function echoLayoutBottom(){

	echo '</body></html>';

}

?>