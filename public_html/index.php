<?php
include('_layout.php');
echoLayoutTop();

include("db-connect.php");
$conn = dbConnect();

$nameErr = "";
$phoneErr = "";
$emailErr = "";
$dueDateErr = "";
$numOfPagesErr = "";
$numOfCopiesErr = "";
$printColorErr = "";
$doubleSidedErr = "";

$validInput = true;
//Get form data to make sure it's valid
$_name = "";
$_phone = "";
$_email = "";
$_dueDate = "";
$_numOfPages = "";
$_numOfCopies = "";

 //Check if the form has been submitted. If it has, start to process the form and save it to the database
 if ($_SERVER['REQUEST_METHOD'] === 'POST'){
	//Define database attributes
	$validInput = true;
 	//Get form data to make sure it's valid
 	$_name = $name = $_POST["name"];
 	$_phone =$phone = $_POST["phone"];
 	$_email = $email = $_POST["email"];
 	$_dueDate = $dueDate = $_POST["dueDate"];
 	$_numOfPages = $numOfPages = $_POST["numOfPages"];
  	$_numOfCopies = $numOfCopies = $_POST["numOfCopies"];
   	$_paperSize = $paperSize = $_POST["paperSize"];
    $_paperColor = $paperColor = $_POST["paperColor"];
   	$_weight = $weight = $_POST["weight"];
    $_finishing = $finishing = $_POST["finishing"];
    $_paymentMethod = $paymentMethod = $_POST["paymentMethod"];

    //validate input
	if (empty($name)) {
		$nameErr = "<br /><span class='red'>This field is required</span>";
		$validInput = false;
	} else {
		if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
			$nameErr = "<br /><span class='red'>Only letters and white space allowed</span>"; 
			$validInput = false;
		}
	}

	if (empty($phone)) {
		$phoneErr = "<br /><span class='red'>This field is required</span>";
		$validInput = false;
	} else {
		if (!preg_match("/^([\(]{1}[0-9]{3}[\)]{1}[ ]{1}[0-9]{3}[\-]{1}[0-9]{4})*$/",$phone)) {
			$phoneErr = "<br /><span class='red'>(XXX) XXX-XXXX format required</span>"; 
			$validInput = false;
		}
	}

	if (empty($_POST["email"])) {
		$emailErr = "<br /><span class='red'>This field is required</span>";
		$validInput = false;
	} else {
		// check if e-mail address is well-formed
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$emailErr = "<br /><span class='red'>Invalid email format</span>";
			$validInput = false;
		}
	}

	if ($dueDate == "") {
		$dueDateErr = "<br /><span class='red'>This field is required</span>";
	}

	if (empty($numOfPages)) {
		$numOfPagesErr = "<br /><span class='red'>This field is required</span>";
		$validInput = false;
	} else {
		if ($numOfPages < 0) {
			$numOfPagesErr = "<br /><span class='red'>Value must be greater then 0</span>"; 
			$validInput = false;
		}
	}

	if (empty($numOfCopies)) {
		$numOfCopiesErr = "<br /><span class='red'>This field is required</span>";
		$validInput = false;
	} else {
		if ($numOfCopies < 0) {
			$numOfCopiesErr = "<br /><span class='red'>Value must be greater then 0</span>"; 
			$validInput = false;
		}
	}

    //set checkbox values
    if(isset($_POST["printColor"])){
   		$printColor = $_POST["printColor"];    	
    }else{
    	$printColor = "N</span>";
    }

    if(isset($_POST["doubleSided"])){
   		$doubleSided = $_POST["doubleSided"];   	
    }else{
    	$doubleSided = "N</span>";
    }

    if($validInput){
    	$fileLocation;

		if (!empty($_FILES["myFile"])) {
			define("UPLOAD_DIR", "//home3/ab78518/public_html/uploads/");
		    $myFile = $_FILES["myFile"];
		 
		    if ($myFile["error"] !== UPLOAD_ERR_OK) {
		        echo "<p>An error occurred.</p>";
		        exit;
		    }
		 
		    // ensure a safe filename
		    $fileName = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
		 
		    // don't overwrite an existing file
		    $i = 0;
		    $parts = pathinfo($fileName);
		    while (file_exists(UPLOAD_DIR . $fileName)) {
		        $i++;
		        $fileName = $parts["filename"] . "-" . $i . "." . $parts["extension"];
		    }
		 
		    // preserve file from temporary directory
		    if(move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $_FILES['myFile']['name'] )){
		        $fileLocation = "uploads/" . $_FILES['myFile']['name'];
		    } else {
		    	$fileLocation = "There was a problem uploading the file.";
		    }
		}

		$sql = "INSERT INTO orders 
		   	(name,
		   	phone,
		   	email,
		    due_date,
		    numOfPages,
		    numOfCopies,
		    paper_size,
		    paper_color,
		    weight,
		    finishing,
		    payment_method,
		    color,
		    doubleSided,
		    img)     
			VALUES 
			('$name',
			'$phone',
			'$email',
		    '$dueDate',
		    '$numOfPages',
		    '$numOfCopies',
		    '$paperSize',
		    '$paperColor',
		    '$weight',
		    '$finishing',
		    '$paymentMethod',
		    '$printColor',
		    '$doubleSided',
		    '$fileLocation')";

		if ($conn->query($sql) === TRUE) 
		{
			echo " new order placed";
			// $email is already set
			$subject = "You order has been received.";
			$message  = "You order has been received.";

			include('mail.php');
			$return = WebmasterMail($email,$subject,$message);			
		} 
		else 
		{
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}
}
?>


<html>

	<head>

		<meta charset="utf-8">
		<title>Flowboard Order Form</title>

		<meta name="description" content="This is the Flowboard Order Form using the Flat UI Toolkit."/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<!-- Loading Bootstrap -->
		<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

		<!-- Loading Flat UI Pro -->
		<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

		<!-- LOADING CUSTOM CSS -->
		<link href="custom_styles.css" rel="stylesheet">

		<link rel="shortcut icon" href="img/favicon.ico">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
			<script src="dist/js/vendor/html5shiv.js"></script>
			<script src="dist/js/vendor/respond.min.js"></script>
		<![endif]-->
		
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>


	<body><!-- style="overflow-y:scroll;" -->
	
		<!-- TOP RIGHT LINKS -->
		<!--<p class="navbar-text pull-right">
			studentprint staff? <a class="navbar-link" href="login.php"> log in</a>
		</p>-->

		<?php
		echoNavLinks();
		?>

		<div class="container">

			<div class="container"><section id="block-text"><div class="block-text">

				<!-- AWESOME TITLE BUT MAYBE FIX THE FORMATTING UP A BIT -->
				<div class="demo-headline">
					<h1 class="title">
						<div class="title"></div>
						StudentPrint
						<small>Order Form</small>
					</h1>
					<br/><br/>
				</div>


				<form action"" enctype='multipart/form-data' method="post">

					<!-- TEXT INPUT FIELDS -->
				
					<?php echo $nameErr ?>
					<div class="form-group">
						<input autofocus="autofocus" type="text" placeholder="Name" name="name" class="form-control"
						value= <?php echo "'$_name'";?> />
					</div>

					<?php echo $phoneErr ?>
					<div class="form-group">
						<input autofocus="autofocus" type="text" placeholder="Phone Number" name="phone" class="form-control" 
						value= <?php echo "'$_phone'";?> />
					</div>
				
					<?php echo $emailErr ?>
					<div class="form-group">
						<input autofocus="autofocus" type="text" placeholder="Email Address" name="email" class="form-control"
						value= <?php echo "'$_email'";?> />
					</div>



					<!-- TODO DATEPICKER WIDGET -->
					<!--<?php echo $dueDateErr ?>
					<div class="form-group form-control">
						<input autofocus="autofocus" type="date" name="dueDate" placeholder="Due Date"
						value= <?php echo "'$_dueDate'";?> />
					</div>-->

					<!-- type = "text gets rid of 2nd datepicker, but makes datepicker nonfunctional..., need date type for backend -->
					<!-- WORKS ON SAFARI BUT IS BROKEN AS HELL IN CHROME WHAT THE SHIT. FIX THIS FUCKER -->
					<?php echo $dueDateErr ?>
					<div class="form-group">
								<div class="input-group">
										<span class="input-group-btn">
											<button class="btn" type="button"><span class="fui-calendar"></span></button>
										</span><!-- type = date or text? -->
										<input autofocus="autofocus" type="date" name="dueDate" class="form-control" value="" placeholder="Need By Date" id="datepicker-01" value= <?php echo "'$_dueDate'";?> />
								</div>
						</div>

		

					<?php echo $numOfPagesErr ?>
					<div class="form-group">
						<input autofocus="autofocus" type="number" placeholder="Number of Pages" name="numOfPages" 
						class="form-control" value= <?php echo "'$_numOfPages'";?> />
					</div>

					<?php echo $numOfCopiesErr ?>
					<div class="form-group">
						<input autofocus="autofocus" type="number" placeholder="Number of Copies" name="numOfCopies" 
						class="form-control" value= <?php echo "'$_numOfCopies'";?> />
					</div>

					<!--
					<?php echo $numOfCopiesErr ?>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Number of Copies</span>
							<input type="text" name="numOfCopies" id="spinner-01" placeholder="" value="1" class="form-control spinner">
						</div>
					</div>
					-->

					<!-- bold color: add select-primary to select class -->

					<?php
					if($_SERVER['REQUEST_METHOD'] === 'POST')
						echo "<option value='$_paperSize'>$_paperSize</option>";?>
					<div class="form-group">
						<select data-toggle="select" class="form-control select select-default" name="paperSize">
							<option value="8.5 x 11in">8.5 x 11 inches</option>
							<option value="8.5 x 14in">8.5 x 14 inches</option>
							<option value="11 x 17in">11 x 17 inches</option>
							<option value="12 x 18in">12 x 18 inches</option>
						</select>
					</div>

					<?php
					if($_SERVER['REQUEST_METHOD'] === 'POST')
						echo "<option value='$_paperColor'>$_paperColor</option>";?>
					<div class="form-group">
						<select data-toggle="select" class="form-control select select-default" name="paperColor">
							<option value = "orchid">Orchid</option>
							<option value = "pink">Pink</option>
							<option value = "yellow">Yellow</option>
							<option value = "gold">Gold</option>
							<option value = "green">Green</option>
							<option value = "buff">Buff</option>
							<option value = "blue">Blue</option>
							<option value = "pulsar pink">Pulsar Pink</option>
							<option value = "fireball fuchsia">Fireball Fuchsia</option>
							<option value = "plasma pink">Plasma Pink</option>
							<option value = "re-entry red">Re-entry Red</option>
							<option value = "rocket red">Rocket Red</option>
							<option value = "cosmic orange">Cosmic Orange</option>
							<option value = "galaxy gold">Galaxy Gold</option>
							<option value = "solar yellow">Solar Yellow</option>
							<option value = "venus violet">Venus Violet</option>
							<option value = "planetary purple">Planetary Purple</option>
							<option value = "celestial blue">Celestial Blue</option>
							<option value = "lunar blue">Lunar Blue</option>
							<option value = "gamma green">Gamma Green</option>
							<option value = "martian green">Martian Green</option>
							<option value = "terra green">Terra Green</option>
							<option value = "lift-off lemmon">Lift-off Lemon</option>
						</select>
					</div>  

					<?php
					if($_SERVER['REQUEST_METHOD'] === 'POST')
						echo "<option value='$_weight'>$_weight</option>";?>
					<div class="form-group">
						<select data-toggle="select" class="form-control select select-default" name="weight">
							<option value="20lbs">20lbs</option>
							<option value="60lbs">60lbs</option>
							<option value="65lbs">65lbs</option>
						</select>
					</div> 


					<?php
					if($_SERVER['REQUEST_METHOD'] === 'POST')
						echo "<option value='$_finishing'>$_finishing</option>";?>
					<div class="form-group">
						<select data-toggle="select" class="form-control select select-default" name="finishing">
							<option value="none">None</option>
							<option value="cutting">Cutting</option>
							<option value="folding">Folding</option>
							<option value="quaters">Quaters</option>
							<option value="binding">Bindings</option>
						</select>
					</div> 

					<?php
					if($_SERVER['REQUEST_METHOD'] === 'POST')
						echo "<option value='$_paymentMethod'>$_paymentMethod</option>";?>
					<div class="form-group">
						<select data-toggle="select" class="form-control select select-default" name="paymentMethod">
							<option value="Cash">Cash</option>
							<option value="Credit">Credit</option>
							<option value="Check">Check</option>
							<option value="Wiscard">Wiscard</option>
						</select>
					</div>  	

					<div class="form-group">
						<?php echo $printColorErr ?>
						<label class="checkbox">
							<input type="checkbox" data-toggle="checkbox" id="checkbox" value="Y" name="printColor" class="checkbox">
							Print in Color
						</label>
					</div>	

					<div class="form-group">
						<?php echo $doubleSidedErr ?>
						<label class="checkbox">
							<input type="checkbox" data-toggle="checkbox" id="checkbox" value="Y" name="doubleSided" class="checkbox">
							Double Sided
						</label>
					</div>

					<!-- REJECTED STYLE
					<div class="bootstrap-switch-square form-group">
						<input type="checkbox" name="printColor" checked data-toggle="switch" id="custom-switch-03" data-on-text="<span class='fui-check'></span>" data-off-text="<span class='fui-cross'></span>" />
						 &nbsp; Print in Color?
					</div>
					
					<div class="bootstrap-switch-square form-group">
						<input type="checkbox" name="doubleSided" checked data-toggle="switch" id="custom-switch-03" data-on-text="<span class='fui-check'></span>" data-off-text="<span class='fui-cross'></span>" />
						 &nbsp; Double Sided?
					</div>	-->
		
					<!-- NOT YET CONNECTED - FILE UPLOAD -->
					<!--<div class="form-group">
						<?php echo  $fileErr ?>
						<span class="fui-clip"></span> &nbsp; &nbsp;Upload File
						<form name="fileUpload" action="mailto:test@gmail.com" method="post">
							<input type="hidden" name="MAX_FILE_SIZE" value="500" />
							<input type="file" name="fileToUpload" id="fileToUpload">
							<button type="file" class="btn btn-primary btn-lg btn-block" name="fileToUpload" id="fileToUpload">Upload File</button>
						</form>
					</div>-->

					<!-- <p>Upload File:         <?php echo  $fileErr ?>     <br> <input type="file" name="fileToUpload" id="fileToUpload"></p> -->


					<!-- REJECTED STYLE
					<div class="form-group">
						<h4><small><div id="display-filename" style="text-align:left">no file selected <?php echo  $fileErr ?></div></small></h4>
						<input type="file" name="fileToUpload" id="fileToUpload" onchange="displayfilename();">
						<button class="btn btn-primary btn-lg btn-block" id="file-button">Upload File</button>
					</div> -->


					<div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<span class="btn btn-primary btn-embossed btn-file">
								<span class="fileinput-new"><span class="fui-upload"></span>&nbsp;&nbsp;Upload File</span>
								<span class="fileinput-exists"><span class="fui-gear"></span>&nbsp;&nbsp;Change File</span>
								<input type="file" name="myFile">
							</span>
							&nbsp;&nbsp;&nbsp;<span class="fileinput-filename"></span>
							<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
						</div>
					</div>


					<!-- NON FUNCTIONAL - "INPUT" WON'T WORK HERE... (also make sure there is no space or newline between textarea tags) -->
					<!--<div class="form-group">
						<textarea class="form-control" name="comment" value="" rows="3" autofocus="autofocus" placeholder="Is there anything else StudentPrint should know about your order?"></textarea>
					</div>-->
								
					<!-- NOT YET CONNECTED - EMAIL NOTIFICATIONS (CHECKBOX) -->
					<!--<div class="form-group">
						<label class="checkbox">
							<input type="checkbox" data-toggle="checkbox" id="checkbox" value="Y" class="checkbox" required>
							Send me notifications about my order via email!
						</label>
					</div>-->
			
					<?php
					require_once('recaptchalib.php');
  					$publickey = "6LdmfQUTAAAAAJDAtw62O599Na10v9cCUiE8go0M"; // You got this from the signup page.
  					echo recaptcha_get_html($publickey);
  					?>
					
					<button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button></div>
					

				</form>

			</div></section></div>

		</div>

		<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
		<script src="../dist/js/vendor/jquery.min.js"></script>
		<script src="../dist/js/vendor/video.js"></script>

		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="../dist/js/flat-ui-pro.min.js"></script>

		<script src="../dist/js/application.js"></script>

		<script>
			$(document).ready(function(){
				$('select[name="inverse-dropdown"], select[name="inverse-dropdown-optgroup"], select[name="inverse-dropdown-disabled"]').select2({dropdownCssClass: 'select-inverse-dropdown'});

				$('select[name="searchfield"]').select2({dropdownCssClass: 'show-select-search'});
				$('select[name="inverse-dropdown-searchfield"]').select2({dropdownCssClass: 'select-inverse-dropdown show-select-search'});
			});
		</script>

		<!-- JANKY FIX FOR AUTOLOADING TO BOTTOM OF PAGE -->
		<script>
			$(document).ready(function() {
			    $(document).scrollTop(0);
			});
		</script>

		<!-- MAKES "PRETTY" FILE UPLOAD POSSIBLE -->
		<!-- REJECTED STYLE
		<script>
			$("#file-button").click(function(){
				$("#fileToUpload").click();
				return false; //THIS IS THE MAGIC THAT KEEPS PAGE FROM RELOADING ON JANKY CLICK, LEAVE IT ALONE
			});
		</script>

		<script>
			// assuming there is a file input with the ID `my-input`...
			function displayfilename(){
				var fileName = document.getElementById("fileToUpload").value;
				var fnSplit = fileName.split(/[\/\\]/);
				fileName = fnSplit[fnSplit.length - 1];
				//alert(fileName);
				document.getElementById('display-filename').innerHTML = "" + fileName;
			};
		</script> -->

	</body>

</html>


<?php
echoLayoutBottom();
?>