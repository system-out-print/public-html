<?php
include('_layout.php');
include("check-if-admin.php");
echoLayoutTop();
?>

<?php 
$usernameErr = "";
$firstnameErr = "";
$lastnameErr = "";
$emailErr = "";
$passwordErr = "";
$confirm_passwordErr = "";

$statusMsg = "";

$empty = "<b class = 'red'> Field cannot be empty</b>";
$validInput = true;

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	//Retrieve entered information
	$first_name = $_POST['firstname'];
	$last_name = $_POST['lastname'];
	$user_name = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$confirm_password = $_POST['confirm_password'];


	if (empty($_POST["username"])) 
	{
		$usernameErr = $empty;
		$validInput = false;
	} 
	else if (!preg_match("/^[a-zA-Z0-9]*$/",$user_name)) 
	{
				$usernameErr = "<b class = 'red'>Only letters and numbers allowed</b>"; 
		$validInput = false;
		}

	if (empty($_POST["firstname"])) 
	{
		$firstnameErr = $empty;	
		$validInput = false;
	} 
	else if (!preg_match("/^[a-zA-Z ]*$/",$first_name)) 
	{
				$firstnameErr = "<b class = 'red'>Only letters and white space allowed</b>"; 
		$validInput = false;
		}

	if (empty($_POST["lastname"])) 
	{
		$lastnameErr = $empty;
		$validInput = false;
	} 
	else if (!preg_match("/^[a-zA-Z ]*$/",$last_name)) 
	{
				$lastnameErr = "<b class = 'red'>Only letters and white space allowed</b>"; 
		$validInput = false;
		}

	if (empty($_POST["email"])) 
	{
		$emailErr = $empty;
		$validInput = false;
	}
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
	{
				$emailErr = "<b class = 'red'>Invalid email format</b>"; 
		$validInput = false;
		}

	if (empty($_POST["password"]))
	{
		$passwordErr = $empty;
		$validInput = false;
	} 
	else if (!preg_match("/^[a-zA-Z0-9]*$/",$password))
	{
				$passwordErr = "<b class = 'red'>Only letters, numbers or symbols</b>"; 
		$validInput = false;
		}

	if (empty($_POST["confirm_password"])) 
	{
		$confirm_passwordErr = $empty;
		$validInput = false;
	}

	if($password != $confirm_password)
	{
		$confirm_passwordErr = "<b class = 'red'>Your passwords do not match</b>";
		$validInput = false;
	}

	if($validInput)
	{
		$password = md5($password); //Encrypt all passwords in database

		include("db-connect.php");
		$conn = dBConnect();
		$sql = "SELECT * FROM `employees`";
		$result = $conn->query($sql);
		
		$user_exists = false;//Set user exists to false
		
		//Fetch defined row in the database table
		if ($result->num_rows > 0) {
			// output data of each row
				while($row = $result->fetch_assoc()) {	
				
				//If username and email is already in the database
				if($row['username'] == $user_name or $row['email'] == $email)
				{
					//Set user exists to true
					$user_exists = true;
					//Display user already exists message
					$statusMsg = "<p class = 'red'>That user name already exists. Please choose a different one.</p>";
				}
			}
		}
		//If user doesn't already exist
		if (!$user_exists)
		{
			//Insert the entered data into database
			$conn->query("INSERT INTO `employees`(`id`,`first_name`,`last_name`,`username`,`email`,`password`) 
					VALUES (NULL,'$first_name','$last_name','$user_name','$email','$password')");
			
			echo "<p>Added Successfully</p><a href='view-users.php'>Go Back</a>";
		}
	}
}
?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Edit User</title>

	<meta name="description" content="This is Flowboard - Edit User using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<div class="container"><section id="block-text"><div class="block-text">

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Add User</small>
				</h1>
				<br/><br/>
			</div>

			<form method="post" class="form-horizontal" role="form"><!-- action"" ? -->

 				<div class="form-group">
	        <label class="col-sm-3 control-label" for="username">Username</label>
	        <div class="col-sm-9">
	          <input type = "text" name = "username" autofocus="autofocus" size = "25" class="form-control"><?php echo $usernameErr;?>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="firstname">First Name</label>
	        <div class="col-sm-9">
	          <input type = "text" name = "firstname" class="form-control" autofocus="autofocus" size = "25"><?php echo $firstnameErr;?>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="lastname">Last Name</label>
	        <div class="col-sm-9">
	          <input type = "text" name = "lastname" autofocus="autofocus" class="form-control" size = "25"><?php echo $lastnameErr;?>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="email">Email</label>
	        <div class="col-sm-9">
	          <input type = "text" name = "email" autofocus="autofocus" class="form-control" size = "25"><?php echo $emailErr;?>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="admin">Password</label>
	        <div class="col-sm-9">
	          <input type = "password" name = "password" autofocus="autofocus" size = "25" class="form-control"><?php echo $passwordErr;?>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="confirm_password">Confirm Password</label>
	        <div class="col-sm-9">
	          <input type = "password" name = "confirm_password" autofocus="autofocus" class="form-control" size = "25" ><?php echo $confirm_passwordErr;?>
	        </div>
	      </div>

	      <div class="form-group"> <!--change value to Edit User? -->
	        <label class="col-sm-3 control-label" for="edituser"></label>
	        <div class="col-sm-9">
	          <button type="submit" class="btn btn-primary btn-lg btn-block" name = "register" size = "25" value = "Register">Create User</button>
	        </div>
	      </div>

			</form>

		</div></section></div>

	</div>



<?php
	echoLayoutBottom();
?>