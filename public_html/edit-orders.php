<?php 
include('_layout.php'); 
include("check-if-login.php");
echoLayoutTop(); ?>

<?php

function displayForm($id,
					$name,
	 		 		$dueDate,
	 		 		$email,
	 		 		$numOfPages,
	 		 		$numOfCopies,
	 		 		$paperSize,
	 		 		$paperColor,
	 		 		$weight,
	 		 		$finishing,
	 		 		$paymentMethod,
	 		 		$printColor,
	 		 		$comment,
	 		 		$status,
	 		 		$error)
{

?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Edit Order</title>

	<meta name="description" content="This is Flowboard - Edit Order using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<div class="container"><section id="block-text"><div class="block-text">

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Edit Order</small>
				</h1>
				<br/><br/>
			</div>

	    <form action"" method="post" class="form-horizontal" role="form">

	    	<input type="hidden" name="id" value="<?php echo $id; ?>"/>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="name">Name</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" name="name" class="form-control" value="<?php echo $name; ?>" />
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="email">Email</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" class="form-control" name="email" value="<?php echo $email; ?>" />
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="dueDate">Due Date</label>
	        <div class="col-sm-9">
						<div class="input-group">
								<span class="input-group-btn">
									<button class="btn" type="button"><span class="fui-calendar"></span></button>
								</span>
								<input autofocus="autofocus" type="datetime" name="dueDate" value="<?php echo $dueDate; ?>" class="form-control" id="datepicker-01" />
						</div>
					</div>
				</div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="numPages">Number of Pages</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" class="form-control" type="number" name="numOfPages" value="<?php echo $numOfPages; ?>"/>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="numCopies">Number of Copies</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" class="form-control" type="number" name="numOfCopies" value="<?php echo $numOfCopies; ?>"/>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="paperSize">Paper Size</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" id="formGroupInputDefault" name="paperSize">
							<option value="<?php echo $paperSize?>"><?php echo $paperSize?></option>
							<option value="8.5 x 11in">8.5 x 11 inches</option>
							<option value="8.5 x 14in">8.5 x 14 inches</option>
							<option value="11 x 17in">11 x 17 inches</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="paperColor">Paper Color</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name="paperColor">
							<option value="<?php echo $paperColor?>"><?php echo $paperColor?></option>
							<option value = "pulsar pink">Pulsar Pink</option>
							<option value = "fireball fuchsia">Fireball Fuchsia</option>
							<option value = "plasma pink">Plasma Pink</option>
							<option value = "re-entry red">Re-entry Red</option>
							<option value = "rocket red">Rocket Red</option>
							<option value = "cosmic orange">Cosmic Orange</option>
							<option value = "galaxy gold">Galaxy Gold</option>
							<option value = "solar yellow">Solar Yellow</option>
							<option value = "venus violet">Venus Violet</option>
							<option value = "planetary purple">Planetary Purple</option>
							<option value = "celestial blue">Celestial Blue</option>
							<option value = "lunar blue">Lunar Blue</option>
							<option value = "gamma green">Gamma Green</option>
							<option value = "martian green">Martian Green</option>
							<option value = "terra green">Terra Green</option>
							<option value = "lift-off lemmon">Lift-off Lemon</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="weight">Weight</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name="weight">
							<option value="<?php echo $weight?>"><?php echo $weight?></option>
							<option value="20lbs">20lbs</option>
							<option value="60lbs">60lbs</option>
							<option value="65lbs">65lbs</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="finishing">Finishing</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name="finishing">
							<option value="<?php echo $finishing?>"><?php echo $finishing?></option>
							<option value="none">None</option>
							<option value="cutting">Cutting</option>
							<option value="folding">Folding</option>
							<option value="quaters">Quaters</option>
							<option value="binding">Bindings</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="paymentMethod">Payment Method</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name="paymentMethod">
							<option value="<?php echo $paymentMethod?>"><?php echo $paymentMethod?></option>
							<option value="Cash">Cash</option>
							<option value="Credit">Credit</option>
							<option value="Check">Check</option>
							<option value="Wiscard">Wiscard</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="printColor">Print in Color?</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name = "printColor">
							<option value="<?php echo $printColor?>"><?php echo $printColor?></option>
							<option value="Black">Black</option>
							<option value="White">White</option>
							<option value="Color">Color</option>
						</select>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="status">Status</label>
	        <div class="col-sm-9">
	          <select data-toggle="select" class="form-control select select-default" name = "status">
							<option value="<?php echo $status?>"><?php echo $status?></option>
							<option value="Recieved">Received</option>
							<option value="In Progress">In Progress</option>
							<option value="Completed">Completed</option>
						</select>	
	        </div>
	      </div>

				<div class="form-group">
	        <label class="col-sm-3 control-label" for="comment">Comments</label>
	        <div class="col-sm-9">
	          <textarea class="form-control" name="comment" value="" rows="3" autofocus="autofocus" placeholder="Notes?"></textarea>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="Edit Order"></label>
	        <div class="col-sm-9">
	          <button type="submit" class="btn btn-primary btn-lg btn-block" id="formGroupInputDefault" value="Edit Order">Edit Order</button>
	        </div>
	      </div>

	    </form>

		</div></section></div>

	</div>

	<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
	<script src="../dist/js/vendor/jquery.min.js"></script>
	<script src="../dist/js/vendor/video.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../dist/js/flat-ui-pro.min.js"></script>

	<script src="../dist/js/application.js"></script>

	<script>
		$(document).ready(function(){
			$('select[name="inverse-dropdown"], select[name="inverse-dropdown-optgroup"], select[name="inverse-dropdown-disabled"]').select2({dropdownCssClass: 'select-inverse-dropdown'});

			$('select[name="searchfield"]').select2({dropdownCssClass: 'show-select-search'});
			$('select[name="inverse-dropdown-searchfield"]').select2({dropdownCssClass: 'select-inverse-dropdown show-select-search'});
		});
	</script>

	<!-- JANKY FIX FOR AUTOLOADING TO BOTTOM OF PAGE -->
	<script>
		$(document).ready(function() {
		    $(document).scrollTop(0);
		});
	</script>

</body>

<?php
//end of function
}

// connect to the database
include('db-connect.php');
$conn = dbConnect();
 
// check if the form has been submitted. If it has, process the form and save it to the database
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{ 

	// confirm that the 'id' value is a valid integer before getting the form data
	if (is_numeric($_POST['id']))
	{

	  	//Get form data to make sure it's valid
	 	$id = $_POST["id"];
	 	$name = $_POST['name'];
	 	$dueDate = $_POST['dueDate'];
	 	$email = $_POST['email'];
	 	$numOfPages = $_POST['numOfPages'];
	 	$numOfCopies = $_POST['numOfCopies'];
	  	$paperSize = $_POST['paperSize'];
	   	$paperColor = $_POST['paperColor'];
	    $weight = $_POST['weight'];
	   	$finishing = $_POST['finishing'];
	    $paymentMethod = $_POST['paymentMethod'];
	    $printColor = $_POST['printColor'];
	    $status = $_POST['status'];
	    $comment = $_POST['comment'];
	 
	    // check that firstname/lastname fields are both filled in
		if ($name == '' || $dueDate == '' || $numOfPages == '' || $numOfCopies == '')
		{
			// generate error message
			$error = 'Please fill in all required fields!';

		    //error, display form
		    displayForm($id,
				       $name,
			 		   $dueDate,
			 		   $email,
			 		   $numOfPages,
			 		   $numOfCopies,
			 		   $paperSize,
			 		   $paperColor,
			 		   $weight,
			 		   $finishing,
			 		   $paymentMethod,
			 		   $printColor,
			 		   $comment,
			 		   $status,
			 		   $error);
		}
		else
		{	
			$sql = "SELECT * FROM orders WHERE id='$id'";
			$result = $conn->query($sql);

			$row = $result->fetch_assoc();

			if ($result->num_rows > 0) {
				if($row['status'] != $status){
					$originalEmail = $row['email'];
					$sub = "Your order status has been updated.";
					$msg = "Your order status has been updated to: $status";

					include('mail.php');
					WebmasterMail($originalEmail, $sub, $msg);
				}

				//update table query	
				$sql = "UPDATE orders SET `name` = '".$name."',
	    								   due_date = '".$dueDate."',
									       numOfPages = '".$numOfPages."',
									       numOfCopies = '".$numOfCopies."',
									       paper_size = '".$paperSize."',
									       paper_color = '".$paperColor."',
									       weight = '".$weight."',
									       finishing = '".$finishing."',
									       payment_method = '".$paymentMethod."',
									       color = '".$printColor."',
									       comments = '".$comment."',
									      `status` = '".$status."' WHERE id = '".$id."'";
				//execute the query update
				$conn->query($sql) ;

				echo "<p>The order has been updated successfuly.</p>
				<a href='view-orders.php'>Go back</a>";

			}
		}
	}
	else
	{
	 	// if the 'id' isn't valid, display an error
	 	echo 'Error!';
	}
}
else
{
 	// if the form hasn't been submitted, get the data from the db and display the form
 
	// get the 'id' value from the URL (if it exists), making sure that it is valid 
	if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)
	{
		// query db
		$id = $_GET['id'];
		$sql = "SELECT * FROM orders WHERE id = '$id'";
 		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		// output data of each row
		    $row = $result->fetch_assoc();
		    // get data from db
			$id = $row['id'];
	 		$name = $row['name'];
		 	$dueDate = $row['due_date'];
		 	$numOfPages = $row['numOfPages'];
		  	$numOfCopies = $row['numOfCopies'];
		   	$paperSize = $row['paper_size'];
		    $paperColor = $row['paper_color'];
		   	$weight = $row['weight'];
		    $finishing = $row['finishing'];
		    $paymentMethod = $row['payment_method'];
		    $printColor = $row['color'];
		    $status = $row['status'];
		    $comment = $row['comments'];
			 
			// show form
			displayForm($id,
				       $name,
			 		   $dueDate,
			 		   $email,
			 		   $numOfPages,
			 		   $numOfCopies,
			 		   $paperSize,
			 		   $paperColor,
			 		   $weight,
			 		   $finishing,
			 		   $paymentMethod,
			 		   $printColor,
			 		   $comment,
			 		   $status,
			 		   '');
		}
	}
	else
	{
	 	// if no match, display result
	 	echo "No results!";
	
	}
}
?>

<?php echoLayoutBottom(); ?>