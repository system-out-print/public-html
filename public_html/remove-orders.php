<?php
session_start();

include("check-if-login.php");

 // connect to the database
include("db-connect.php");
$conn = dBConnect();
 
 // check if the 'id' variable is set in URL, and check that it is valid
 if (isset($_GET['id']) && is_numeric($_GET['id']))
 {
 	// get id value
 	$id = $_GET['id'];
 
 	// delete the entry
 	$result =  $conn->query("DELETE FROM orders WHERE id = '$id'"); 
 
 	// redirect back to the order page
 	header("Location: view-orders.php");

 }
 else
 {
 	// if id isn't set, or isn't valid, redirect back to view page
 	header("Location: view-orders.php");  
 }
 
?>