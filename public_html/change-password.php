<?php 
include('_layout.php'); 
echoLayoutTop();
include("check-if-login.php");
?>



<?php
$empty = "";
$changed = "";
$confirm = "";
$wrong = "";

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	//Get POST variables
	$username = $_SESSION['username'];
	$OldPass = $_POST['OldPass'];
	$NewPass = $_POST['NewPass'];
	$ReNewPass = $_POST['ReNewPass'];
	
	include('db-connect.php');
	$conn = dbConnect();

	//has the password
	$HashedOldPass = md5($OldPass);

	//query the database searching for a row that has $userID and $oldPass
	$sql = "SELECT * FROM `employees` WHERE `username` = '$username' AND `password` = '$HashedOldPass'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		//If the passwords match assign the user a temporary password
		if($NewPass == $ReNewPass)
		{
			$hashedPass = md5($NewPass); //Hash the temporary password in the database
			$sql = "UPDATE employees SET password='$hashedPass' WHERE username='$username'"; //Update the database with the temporary password
			$conn->query($sql);
			if ($conn->query($sql) === TRUE) {
					$changed = "Your password has been changed.";
			} else {
					$changed = "Error updating record: " . $conn->error;
			}
			
		}
		else
		{
			$confirm = "Your confirmed password did not match.";
		}
	} 
	else 
	{
		$wrong = "Enter your old password.";
	}
	
}
?>

<?php

if (isset($_SESSION['username']))
{

?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Edit User</title>

	<meta name="description" content="This is Flowboard - Edit User using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<div class="container"><section id="bigger-block-text"><div class="bigger-block-text">

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Change Password</small>
				</h1>
				<br/><br/>
			</div>

		</div></section>
		<section id="block-text"><div class="block-text">

			<form method="POST" class="form-horizontal" role="form"><!-- action"" ? -->

				<div class="form-group">
					<label class="col-sm-4 control-label" for="OldPass">Current Password</label>
					<div class="col-sm-6">
						<input type = "password" name = "OldPass" autofocus="autofocus" class="form-control"><?php echo $empty;?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label" for="NewPass">New Password</label>
					<div class="col-sm-6">
						<input type = "password" name = "NewPass" class="form-control" autofocus="autofocus"><?php echo $empty;?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label" for="ReNewPass">Confirm New Password</label>
					<div class="col-sm-6">
						<input type = "password" name = "ReNewPass" class="form-control" autofocus="autofocus"><?php echo $empty;?>
					</div>
				</div>

	      <div class="form-group"> <!--change value to Edit User? -->
	        <label class="col-sm-4 control-label" for="edituser"></label>
	        <div class="col-sm-6">
	          <button type="submit" class="btn btn-primary btn-lg btn-block" value="Submit">Change Password</button>
	        </div>
	      </div>

				<h4><?php echo $changed; echo $wrong; echo $confirm?></h4>

			</form>

		</div></section></div>

	</div>

<?php

}
else
{

?>
	<div class = "container page">

	<p>You must <a href = "login.php">login</a> to change your password</p> 

	</div>

<?php

}

?>

<?php echoLayoutBottom();?>

</body>
</html>