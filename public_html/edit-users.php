<?php 
include('_layout.php');
include("check-if-admin.php");
echoLayoutTop(); ?>

<?php

function displayForm($id,
					$firstname,
	 		 		$lastname,
	 		 		$username,
	 		 		$email,
	 		 		$admin,
	 		 		$error)
{ ?>

<head>

	<meta charset="utf-8">
	<title>Flowboard - Edit User</title>

	<meta name="description" content="This is Flowboard - Edit User using the Flat UI Toolkit."/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- Loading Bootstrap -->
	<link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

	<!-- Loading Flat UI Pro -->
	<link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

	<!-- LOADING CUSTOM CSS -->
	<link href="custom_styles.css" rel="stylesheet">

	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="dist/js/vendor/html5shiv.js"></script>
		<script src="dist/js/vendor/respond.min.js"></script>
	<![endif]-->
</head>

<body style="overflow-y:scroll;">

	<?php
	echoNavLinks();
	?>

	<div class="container">

		<div class="container"><section id="block-text"><div class="block-text">

			<div class="demo-headline">
				<h1 class="title">
					<div class="title"></div>
					StudentPrint
					<small>Edit User</small>
				</h1>
				<br/><br/>
			</div>

	    <form action"" method="post" class="form-horizontal" role="form">

	    	<input type="hidden" name="id" value="<?php echo $id; ?>"/>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="name">First Name</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" name="firstname" class="form-control" value="<?php echo $firstname; ?>" />
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="name">Last Name</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" class="form-control" name="lastname" value="<?php echo $lastname; ?>" />
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="username">Username</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" class="form-control" name="username" value="<?php echo $username; ?>"/>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="email">Email</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" class="form-control" name="email" value="<?php echo $email; ?>"/>
	        </div>
	      </div>

	      <div class="form-group">
	        <label class="col-sm-3 control-label" for="admin">Admin?</label>
	        <div class="col-sm-9">
	          <input autofocus="autofocus" type="text" id="formGroupInputDefault" class="form-control" name="admin" value="<?php echo $admin; ?>" />
	        </div>
	      </div>

	      <div class="form-group"> <!--change value to Edit User? -->
	        <label class="col-sm-3 control-label" for="edituser"></label>
	        <div class="col-sm-9">
	          <button type="submit" class="btn btn-primary btn-lg btn-block" id="formGroupInputDefault" value="Edit Order">Edit User</button>
	        </div>
	      </div>

	    </form>

		</div></section></div>

	</div>

	<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
	<script src="../dist/js/vendor/jquery.min.js"></script>
	<script src="../dist/js/vendor/video.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../dist/js/flat-ui-pro.min.js"></script>

	<script src="../dist/js/application.js"></script>

	<script>
		$(document).ready(function(){
			$('select[name="inverse-dropdown"], select[name="inverse-dropdown-optgroup"], select[name="inverse-dropdown-disabled"]').select2({dropdownCssClass: 'select-inverse-dropdown'});

			$('select[name="searchfield"]').select2({dropdownCssClass: 'show-select-search'});
			$('select[name="inverse-dropdown-searchfield"]').select2({dropdownCssClass: 'select-inverse-dropdown show-select-search'});
		});
	</script>

	<!-- JANKY FIX FOR AUTOLOADING TO BOTTOM OF PAGE -->
	<script>
		$(document).ready(function() {
		    $(document).scrollTop(0);
		});
	</script>

</body>
<?php }

// connect to the database
include('db-connect.php');
$conn = dbConnect();
 
// check if the form has been submitted. If it has, process the form and save it to the database
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{ 

	// confirm that the 'id' value is a valid integer before getting the form data
	if (is_numeric($_POST['id']))
	{

	  	//Get form data to make sure it's valid
	 	$id = $_POST["id"];
	 	$firstname = $_POST['firstname'];
	 	$lastname = $_POST['lastname'];
	 	$username = $_POST['username'];
	 	$email = $_POST['email'];
	 	$admin = $_POST['admin'];

	    // check that firstname/lastname fields are both filled in
		if ($firstname == '' || $lastname == '' || $username == '' || $email == '' || $admin == '')
		{
			// generate error message
			$error = 'Please fill in all required fields!';

		    //error, display form
		    displayForm($id,
				       $firstname,
			 		   $lastname,
			 		   $username,
			 		   $email,
			 		   $admin,
			 		   $error);
		}
		else
		{
			//update table query	
			$sql = "UPDATE employees 
				SET first_name = '$firstname', last_name = '$lastname' , username = '$username', 
				email = '$email', admin='$admin' WHERE id='$id'";
			//execute the query update
			$conn->query($sql);

			echo "<p>Updated Successfully</p><a href='view-users.php'>Go Back</a>";
		}
	}
	else
	{
	 	// if the 'id' isn't valid, display an error
	 	echo 'Error!';
	}
}
else
{
 	// if the form hasn't been submitted, get the data from the db and display the form
 
	// get the 'id' value from the URL (if it exists), making sure that it is valid 
	if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0)
	{
		// query db
		$id = $_GET['id'];
		$sql = ("SELECT * FROM employees WHERE id = '$id'") ; 
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		// output data of each row
		    $row = $result->fetch_assoc();
		 
			// get data from db
			$id = $row['id'];
	 		$firstname = $row['first_name'];
		 	$lastname = $row['last_name'];
		 	$username = $row['username'];
		 	$email = $row['email'];
		 	$admin = $row['admin'];


		 	$result = "SELECT * FROM employees"; 
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			// output data of each row
			    $row = $result->fetch_assoc();
			}
			 
			// show form
			displayForm($id,
				       $firstname,
			 		   $lastname,
			 		   $username,
			 		   $email,
			 		   $admin,
			 		   '');
		}
		else
		{
		 	// if no match, display result
		 	echo "No results!";
		}
	}
	else
	{
	 	// if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
	 	echo 'Error!';
	}
}
?>

<?php echoLayoutBottom(); ?>