<?php
ob_start();
session_start();
?>

<html>

  <head>

    <meta charset="utf-8">
    <title>Flowboard Order Form</title>

    <meta name="description" content="This is the Flowboard Order Form using the Flat UI Toolkit."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Loading Bootstrap -->
    <link href="../dist/css/vendor/bootstrap.min.css" rel="stylesheet">

    <!-- Loading Flat UI Pro -->
    <link href="../dist/css/flat-ui-pro.css" rel="stylesheet">

    <!-- LOADING CUSTOM CSS -->
    <link href="custom_styles.css" rel="stylesheet">

    <link rel="shortcut icon" href="img/favicon.ico">

  </head>


  <body>

    <!--<p class="navbar-text pull-right">
      <a class="navbar-link" href="#">Do we want links here?</a> |
      <a class="navbar-link" href="index.php">Maybe just back to the main page?</a>
    </p>-->

    <?php
    include('_layout.php');
    echoNavLinks();
    ?>

    <div class="container">

      <div class="container"><section id="block-text"><div class="block-text">

        <div class="demo-headline">
          <h1 class="title">
            <div class="title"></div>
            StudentPrint
            <small>Log In</small>
          </h1>
          <br/><br/>
        </div>   


        <form class="new_user" id="new_user" action="login.php" accept-charset="UTF-8" method="post">
          <input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="sdlSY8nPkjyOwzmxME4evFRH3OzG849c37paiTGBWFhOL2Canbz94vDLmFgADDqDr0+NZVwFzEfu3rDyIf3ADQ==" />

          <div class="order-form"> <!--"col-sm-6 col-md-4"> -->

            <div id="login">
              <form action="javascript:void(0);" method="get">
                <fieldset>

                  <div class="form-group">
                    <input type="text" required value="Username" onBlur="if(this.value=='')this.value=''" onFocus="if(this.value=='Username')this.value='' " autofocus="autofocus" type="text" value="" placeholder="Username" class="form-control" name="username" id="user_email" required maxlength = "50">
                  </div>

                  <div class="form-group">
                    <input type="password" required value="Password" onBlur="if(this.value=='')this.value=''" onFocus="if(this.value=='Password')this.value='' " input autocomplete="off" autofocus="autofocus" type="text" value="" placeholder="Password" class="form-control" name="password" id="user_password" required maxlength = "50">
                  </div>

                  <div class="form-group"><button type="submit" value="Login" class="btn btn-primary btn-lg btn-block">Log In</button></div>

                  <!-- I WANNA BE A REAL LINK! -->
                  <p><a href="reset-password.php">Forgot password?</a></p>  

                  <?php 
                  if ($_SERVER['REQUEST_METHOD'] === 'POST')
                  {
                     //Define database attributes
                    include("db-connect.php");
                    $conn = dBConnect();

                    //Get form data to make sure it's valid
                    $username = $_POST['username'];
                    $password = $_POST['password'];
                    $password = md5($password);

                    $sql = "SELECT * FROM employees";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                      // output data of each row
                        while($row = $result->fetch_assoc()) 
                        { 
                        
                        //If username and email is already in the database
                        if($row['username'] == $username and $row['password'] == $password)
                        {
                          $_SESSION['loggedIn'] = 1;
                          $_SESSION['admin'] = $row['admin'];
                          $_SESSION['username'] = $username;
                        }
                      }
                    }

                    if(isset($_SESSION['loggedIn']))
                    {
                      if($_SESSION['loggedIn'] == 1)
                      {
                        header("location: index.php");
                      }
                    }
                    else
                    {
                      echo "<font color='red'>Login failed, please try again!</font>";
                      
                    }
                  }
                  ?> 
                
                </fieldset>
              </form>
            </div> <!-- end login -->

          </div>
        </form>

      </div></section></div>

    </div>

   <!-- THIS IS PRETTY MUCH STRAIGHT FROM THE FLAT UI DEMO - YAY FOOTERS-->

  <!--
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-xs-7">
            <h3 class="footer-title">Anton is a Lizard</h3>
            <p>This is some text.<br>
              Here is some more text.<br>
              For real, you gotta check out allllllll this text. <br>
              Go to: <a href="http://flowboard.herokuapp.com" target="_blank">flowboard.herokuapp.com</a>
            </p>
          </div>

          <div class="col-xs-5">
            <div class="footer-banner">
              <h3 class="footer-title">Here's a list of things</h3>
              <ul>
                <li>We could credit sources</li>
                <li>Or link to stuff in progress</li>
                <li>Or debugging and testing shit</li>
                <li>Or maybe just everybody's names</li>
                <li>Idk but damn this list looks excellent</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  -->


  </body>
</html>